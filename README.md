## Hangman Game in Java

This is a simple Hangman game implemented in Java. 

To play the game, run the `HangMan` class. Guess the letters to uncover the hidden word, but be careful - you have limited attempts before the hanging machine is complete!

[YouTube Demo](https://www.youtube.com/watch?v=cXtTG8cPVHQ)

Enjoy the game!
