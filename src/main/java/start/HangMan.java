package start;



import java.util.Scanner;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class HangMan {
    public static void main(String[] args) {

        HangingMachine hanging = new HangingMachine();

        List<String> words = Arrays.asList("kreda", "kartka", "butelka", "rekin", "manekin", "papieros", "fajka", "spodnie", "marka", "popcorn");

        Random roll = new Random();

        String randomWord = words.get(roll.nextInt(words.size()));


        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome to Hangman Game!");
        System.out.println(" ");


        char[] letters = new char[randomWord.length()];

        Arrays.fill(letters, '_');

        int maxAttempts = 8;


        while (maxAttempts > 0) {
            System.out.println("Password what u looking for:  "+ String.valueOf(letters));

            System.out.println();
            System.out.println("Enter new letter to find the password:  ");
            char guessesLetter = scanner.next().toLowerCase().charAt(0);
            boolean letterMatch = false;

            for (int i = 0; i < randomWord.length(); i++) {
                if (randomWord.charAt(i) == guessesLetter) {
                    letterMatch = true;
                    letters[i] = guessesLetter;
                }
            }
            if(!letterMatch) {
                maxAttempts--;
                System.out.println("Wrong letter try again! You have " + maxAttempts + " left." );
                hanging.hangingMachine(maxAttempts);

            }
            if(maxAttempts == 0){
                System.out.println("You are hanging! You lose the game!");
                System.out.println("Password was: "+randomWord);
            }
            boolean winTheGame = true;
            for(char i :letters){
                if(i == '_'){
                    winTheGame = false;
                    break;
                }
            }
            if(winTheGame){
                System.out.println("You win the game! Congratulations");
                break;
            }

        }
        scanner.close();
    }


}
